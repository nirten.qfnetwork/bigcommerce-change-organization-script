var productContainer = document.querySelector(".productView");
var description_wrapper = document.querySelector('.product-description')
let articals = document.querySelectorAll(".productView-description");

// Check whether artical is additional information or for description
var Addition_info_area = "";
var description = "";

articals.forEach(element=>{
    let artical = element.querySelector('.productView-description .productView-title');
    if(artical.innerText == "Extra Information"){

        Addition_info_area = artical?artical:null;
    }
    else
    {
        description = artical?artical:null;
    }
});



var brand = document.querySelector('.productView-brand');
var sku = BCData.product_attributes.sku;
var MPN = BCData.product_attributes.mpn;
var GTIN = BCData.product_attributes.gtin;
var Specifications = "";






function showLoader() {
    const svg = `<svg id="svg__loader" style="padding: 1rem;">
                            <circle fill="#4E4E4E" stroke="none" cx="6" cy="50" r="6">
                            <animateTransform
                                attributeName="transform"
                                dur="1s"
                                type="translate"
                                values="0 15 ; 0 -15; 0 15"
                                repeatCount="indefinite"
                                begin="0.1"/>
                            </circle>
                            <circle fill="#4E4E4E" stroke="none" cx="30" cy="50" r="6">
                            <animateTransform
                                attributeName="transform"
                                dur="1s"
                                type="translate"
                                values="0 10 ; 0 -10; 0 10"
                                repeatCount="indefinite"
                                begin="0.2"/>
                            </circle>
                            <circle fill="#4E4E4E" stroke="none" cx="54" cy="50" r="6">
                            <animateTransform
                                attributeName="transform"
                                dur="1s"
                                type="translate"
                                values="0 5 ; 0 -5; 0 5"
                                repeatCount="indefinite"
                                begin="0.3"/>
                            </circle>
                        </svg>`;
    productContainer.parentElement.insertAdjacentHTML('beforeend', svg)
}


function hideLoader() {
    const loader = document.querySelector('#svg__loader')
    loader?.remove()
}


function hideDescription() {
    if (description && description.querySelector('p')) {
        description.style.display = 'none'
    }
}


function showDescription() {
    if (description && description.querySelector('p')) {
        description.style.display = 'block'
    }
}


async function revert() {
    showDescription()
}




async function showProductStory(productStories) {
    const [{ URL }] = productStories;
    const iframe = document.createElement('iframe');
    console.log("Product Story Url :", URL);

    if (URL) {
        try {
            // showLoader();
            const response = await fetch(URL);
            const productStory = await response.text();

            let description_body = document.createElement("article");
            description_body.classList.add("productView-description");
            description_body.id = "productView-description";
            description_body.innerHTML += "<h2 class='productView-title' >Product Story</h2>";

            // Append iframe to description_body
            description_body.appendChild(iframe);

            // Set iframe attributes
            iframe.src = URL;
            iframe.width = "100%";
            iframe.height = "400px"; // Set initial height or adjust as needed
            iframe.style.border = "none"; // Optional: remove iframe border

            // Append productStory to description_body (if needed)
            description_body.innerHTML += productStory;

            // Append description_body to description_wrapper
            description_wrapper.insertAdjacentElement('beforeend', description_body);

            // Resize the iframe to take full height w.r.t its content
            function resize() {
                iframe.height = iframe.contentWindow.document.documentElement.scrollHeight + 'px';
            }

            // Call resize after the iframe loads
            iframe.onload = resize;

        } finally {
            // hideLoader(); // hide the loader when content is loaded
        }
    }
}




async function showResonsToBuy(resonsToBuy) {


    // console.log("showReasontobuy working");/

    let html = '';
    let products = ""
    // const iframe = document.createElement('iframe')
    // iframe.width = '100%'
    // iframe.frameBorder = 0
    // iframe.scrolling = 'no'
    // iframe.onload = resize


    for (const product of resonsToBuy) {
        products += `<div class="row mb-3">
                                <div class="col-md-6">
                                    <h3>${product.Title}</h3>
                                    <p>${product.Value}</p>
                                </div>
                                <div class="col-md-6">
                                    <img src="${product.HighPic}" style="max-width:100%">
                                </div>
                            </div>\n`
    }
    html += `<div class="container-fluid">${products}</div>`

    // iframe.srcdoc = html


    let description_body = document.createElement("article");
    description_body.classList.add("productView-description");
    description_body.id = "productView-description";
    description_body.innerHTML += "<h2 class='productView-title' >Reason to buy</h2>";
    description_body.innerHTML += html;

    // Set attributes for iframe


    description_wrapper.insertAdjacentElement('beforeend', description_body);

    // console.log("Reason To Buy", html);








    hideLoader() // hide the loader when content is loaded


    function resize() {
        iframe.width = "100%";
        iframe.height = iframe.contentWindow.document.documentElement.scrollHeight + 'px'
        // section.style.position = "static";
        // iframe.style.visibility = "visible";
    }






}


async function fetchProductByGTIN(GTIN) {
    try {
        showLoader()
        const response = await fetch(`https://live.icecat.biz/api?UserName=epiceasy&Language=en&GTIN=${GTIN}`);
        const data = await response.json();
        //extracting the product part
        const { data: product } = data;
        return product;
    } catch {
        return null
    } finally {
        hideLoader()
    }
}


async function fetchProductByBrandNameSKU(brand, sku) {
    try {
        showLoader()
        const response = await fetch(`https://live.icecat.biz/api?UserName=epiceasy&Language=en&brand=${brand}&ProductCode=${sku}`);
        const data = await response.json();
        //extracting the product part
        const { data: product } = data;
        return product;
    } catch {
        return null
    } finally {
        hideLoader()
    }
}


async function init() {


    let product = null


    if (!productContainer) {
        return;
    }


    if (GTIN && GTIN.trim() != "") {
        product = await fetchProductByGTIN(GTIN.trim())
    } else if (brand && MPN && brand.innerText.trim() !== '' && MPN.trim() !== '') {
        product = await fetchProductByBrandNameSKU(brand.innerText.trim(), MPN.trim())
    } else {
        return
    }


    hideDescription()
    if (!product) {
        showDescription()
        return
    }
    if (product.ProductStory.length > 0) {
        showProductStory(product.ProductStory)
    } else if (product.ReasonsToBuy.length > 0) {
        showResonsToBuy(product.ReasonsToBuy)
    } else {
        revert()
    }


    if (product.FeaturesGroups.length > 0) {
        showspecification(product.FeaturesGroups);
    }
}


function showspecification(FeaturesGroups) {








    Specifications += "<h2 class='productView-title'>Extra Information</h2>";

    for (const iterator of FeaturesGroups) {
        var Features = iterator.Features; // collection of features
        // console.log(Features);


        table = `<table class="table"><thead>
                    <tr>
                        <th colspan="2" style="color:white" >${iterator.FeatureGroup.Name.Value}</th>
                    </tr>
                    </thead>


                    <tbody>`;


        table += GetfeaturesValues(Features);


        table += `</tbody>
                </table>
                    `;
        Specifications +=(table);
    }



    
    Addition_info_area.parentElement.innerHTML =  Specifications.toString();

}




function GetfeaturesValues(Features = null) {


    if (Features.length > 0) {


        var tr = "";


        for (const iterator of Features) {
            tr += `<tr><td>${iterator.Feature.Name.Value}</td><td>${iterator.Value}</td></tr>`;
        }
        return tr;
    }
    else {
        return "<tr><td>No Features Found</td></tr>";
    }


}




document.addEventListener("DOMContentLoaded", () => {
    init()
});


